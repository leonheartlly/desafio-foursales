# desafio-foursales

Desenvolver API REST utilizando Java e Spring Boot p/ controle de candidatos e cartões de crédito, nesta API deve conter rotas CRUD p/ controle dos candidatos e p/ controle dos cartões de créditos dos candidatos, o projeto deve seguir até o ponto que salvaria os dados no banco de dados utilizando o Hibernate.
Requisitos:
- Criar camada de segurança sobre todas as rotas do sistema e deixar somente a rota para deletar um candidato pública.



###Implementação

Tomei a liberdade de liberar rotas para o banco em memoria e swagger, visando uma análise  mais fácil do projeto:

 - H2: http://localhost:8080/h2-console
   - user: sa
   - password: 123
 - Swagger: http://localhost:8080/swagger-ui.html


Para obter o token de autenticacao:
   - curl --location --request POST 'localhost:8080/login' \
     --header 'Content-Type: application/json' \
     --data-raw '{
     "email": "joao.silva@outlook.com",
     "password": "456789123"
     }'
   - O token estará no header da resposta do /login.

   - como estou usando o banco em memoria, a classe DBConfiguration faz um insert na tabela ao iniciar. Assim, já existem os usuários a seguir:
      - <b>Email:</b> joao.silva@outlook.com <b>Senha:</b> 456789123
      - <b>Email:</b> zezin.souza@gmail.com <b>Senha:</b> 159753
      - <b>Email:</b> queen.france@outlook.com <b>Senha:</b> 456789123
      
      
# POSSIVEIS MELHORIAS

   - Implementar um custom validator para os casos.
   - proibir e permitir acesso a usuarios de acordo com perfil.
   - Restricao de conteudos por cliente.
   - validacao de dados de entrada.
   - melhor tratativa das exceptions.
   - Criação de Projections para obter retornos costumizados a cada situacao.
     
 

