package com.example.demo.service.Exceptions;


public class DataIntegrityException extends RuntimeException {

    /**
     * Recebe mensagem de excessão.
     *
     * @param msg
     */
    public DataIntegrityException( String msg ) {

        super( msg );

    }


    /**
     * Recebe a mensagem da excessão e uma causa ocorrida antes e lançada.
     *
     * @param msg
     *            mensagem da excessão.
     * @param cause
     *            Causa lançada recebida.
     */
    public DataIntegrityException( String msg, Throwable cause ) {

        super( msg, cause );
    }
}
