package com.example.demo.service;

import com.example.demo.domain.Client;
import com.example.demo.repository.ClientRepository;
import com.example.demo.service.Exceptions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserAuthenticationService implements UserDetailsService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Client> cli = clientRepository.findByEmail(email);
        return cli.orElseThrow(() -> new BusinessException("User is not registered."));
    }
}
