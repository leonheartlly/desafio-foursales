package com.example.demo.service;

import com.example.demo.domain.Client;
import com.example.demo.dto.ClientDTO;
import com.example.demo.repository.ClientRepository;
import com.example.demo.service.Exceptions.BusinessException;
import com.example.demo.service.Exceptions.DataIntegrityException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class ClientService {

    private final ClientRepository clienteRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public ClientService(ClientRepository clienteRepository, BCryptPasswordEncoder passwordEncoder) {
        this.clienteRepository = clienteRepository;
        this.passwordEncoder = passwordEncoder;
    }


    public List<Client> listAll() {
        return clienteRepository.findAll();
    }

    public Client findByEmail(String email) {
        return clienteRepository.findByEmail(email).orElseThrow(() -> new BusinessException("Cliente solicitado não existe."));
    }


    public Client findById(Long id) {

        Optional<Client> client = clienteRepository.findById(id);

        return client.orElseThrow(
                () -> new BusinessException("O objeto solicitado não foi encontrado. Id: " + id + ", Tipo: " + Client.class.getName())
        );
    }

    @Transactional
    public Client insert(Client client) {

        client.setId(null);
        client.setPassword(passwordEncoder.encode(client.getPassword()));
        client.setCpf(passwordEncoder.encode(client.getCpf()));

        return clienteRepository.save(client);
    }

    public void delete(Long id) {

        try {
            findById(id);

            clienteRepository.deleteById(id);
        } catch (BusinessException onfe) {
            log.error(onfe);
            throw new BusinessException("Não foi possivel efetuar a remoção.");
        } catch (DataIntegrityViolationException dive) {
            log.error(dive);
            throw new DataIntegrityException("Não foi possível efetuar a remoção.");
        }
    }


    public Client update(Client cliente) {

        Client old = findById(cliente.getId());

        updateData(old, cliente);
        return clienteRepository.save(old);
    }

    private void updateData(Client oldCliente, Client newCliente) {

        oldCliente.setEmail(newCliente.getEmail());
        oldCliente.setBirthday(newCliente.getBirthday());
        oldCliente.setName(newCliente.getName());
        oldCliente.setCpf(newCliente.getCpf());
        oldCliente.setRg(newCliente.getRg());
    }


    public Client convertDTO(ClientDTO casinoDTO) {
        return Client.builder()
                .name(casinoDTO.getName())
                .email(casinoDTO.getEmail())
                .birthday(casinoDTO.getBirthday())
                .cpf(casinoDTO.getCpf())
                .rg(casinoDTO.getRg())
                .password(passwordEncoder.encode(casinoDTO.getPassword()))
                .build();
    }
}
