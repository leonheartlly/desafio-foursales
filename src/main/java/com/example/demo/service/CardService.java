package com.example.demo.service;

import com.example.demo.domain.Card;
import com.example.demo.domain.Client;
import com.example.demo.domain.enuns.Flags;
import com.example.demo.dto.CardDTO;
import com.example.demo.dto.NewCardDTO;
import com.example.demo.repository.CardRepository;
import com.example.demo.service.Exceptions.BusinessException;
import com.example.demo.service.Exceptions.DataIntegrityException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Log4j2
@Service
public class CardService {


    private final CardRepository cardRepository;
    private final ClientService clientService;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public CardService(CardRepository cardRepository, BCryptPasswordEncoder passwordEncoder, ClientService clientService) {
        this.cardRepository = cardRepository;
        this.passwordEncoder = passwordEncoder;
        this.clientService = clientService;
    }


    public List<Card> listAll() {

        List<Card> cards = cardRepository.findAll();

        return cards;
    }

    public List<Flags> listAllStatus() {

        List<Flags> cards = Arrays.asList(Flags.values());

        return cards;
    }

    public List<Card> findByClientId(Long id) {

        Optional<List<Card>> cardSet = cardRepository.findByClientId(id);

        return cardSet.orElseThrow(
                () -> {
                    log.error("O Cliente id {} nao encontrado na base de dados.", id);
                    return new BusinessException("O usuário para este cartão não foi encontrado. Id: " + id + ", Tipo: " + Card.class.getSimpleName());
                });
    }

    public Card findById(Long id) {

        Optional<Card> card = cardRepository.findById(id);

        return card.orElseThrow(
                () -> new BusinessException("O objeto solicitado não foi encontrado. Id: " + id + ", Tipo: " + Card.class.getSimpleName())
        );
    }

    @Transactional
    public Card insert(NewCardDTO cardDTO) {

        Client cli = clientService.findById(cardDTO.getId());
        cardDTO.setPassword(passwordEncoder.encode(cardDTO.getPassword()));

        Card card = Card.builder()
                .password(cardDTO.getPassword())
                .cvv(generateCVVNumber())
                .cpf(cli.getCpf())
                .cardNumber(generateCardNumber())
                .displayName(cli.getName())
                .flag(cardDTO.getFlag().getDesc())
                .client(cli)
                .build();

        log.info("Inserido novo cartao para o usuario: {}", cli.getName());

        return cardRepository.save(card);
    }

    public void delete(Long id) {

        try {
            Card card = findById(id);
            log.info("Cartao numero: {} foi encontrado e sera removido.", card.getCardNumber());
            cardRepository.deleteById(id);
        } catch (DataIntegrityViolationException dive) {
            log.error(dive);
            throw new DataIntegrityException("Não foi possível efetuar a remoção.");
        }
    }

    @Transactional
    public Card update(Card card) {

        Card old = findById(card.getId());

        updateData(old, card);
        return cardRepository.save(old);
    }

    private void updateData(Card oldPedido, Card newCartao) {

        oldPedido.setPassword(newCartao.getPassword());
        oldPedido.setStatus(newCartao.getStatus());
    }

    public Card convertDTO(CardDTO cardDTO) {
        return Card.builder()
                .status(cardDTO.getStatus())
                .cardNumber(cardDTO.getCardNumber())
                .cpf(cardDTO.getCpf())
                .flag(cardDTO.getFlag())
                .expirationDate(cardDTO.getExpirationDate())
                .cvv(cardDTO.getCvv())
                .build();
    }

    private int generateCVVNumber() {
        Random rand = new Random();
        int low = 100;
        int high = 999;
        return rand.nextInt(high - low) + low;
    }

    private String generateCardNumber() {
        StringBuilder builder = new StringBuilder();
        Random rand = new Random();

        int low = 1000;
        int high = 9999;
        for (int i = 0; i < 4; i++) {
            if (i != 0)
                builder.append(" ");
            builder.append(rand.nextInt(high - low) + low);
        }
        log.debug("Gerado novo cartao numero {} ", builder.toString());
        return builder.toString();
    }

}
