package com.example.demo.dto;

import com.example.demo.domain.enuns.Flags;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NewCardDTO implements Serializable {

    private Long id;

    private String password;

    private Flags flag;

    public NewCardDTO(com.example.demo.domain.Card card) {
        this.id = card.getId();
        this.password = card.getPassword();
        this.flag = Flags.getPedidoStatus(card.getFlag());
    }

    @Override
    public String toString() {
        return "NewCardDTO{" +
                "id=" + id +
                ", password='" + password + '\'' +
                ", flag=" + flag +
                '}';
    }
}
