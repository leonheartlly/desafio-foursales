package com.example.demo.dto;


import com.example.demo.domain.Card;
import com.example.demo.domain.enuns.CardStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CardDTO {

    @JsonIgnore
    private Long id;

    private String displayName;

    @JsonIgnore
    private String cpf;

    private CardStatus status;

    private String cardNumber;

    private int cvv;

    @JsonIgnore
    private String password;

    private String flag;

    @JsonIgnore
    private Integer attempt;

    private LocalDate expirationDate;

    @JsonIgnore
    private LocalDate creationDate;

    public CardDTO(Card card) {
        this.displayName = card.getDisplayName();
        this.cpf = card.getCpf();
        this.status = card.getStatus();
        this.cardNumber = card.getCardNumber();
        this.cvv = card.getCvv();
        this.password = card.getPassword();
        this.flag = card.getFlag();
        this.attempt = card.getAttempt();
        this.expirationDate = card.getExpirationDate();
    }


}
