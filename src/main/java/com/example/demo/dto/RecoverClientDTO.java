package com.example.demo.dto;

import com.example.demo.domain.Client;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RecoverClientDTO {

    private String name;

    private LocalDate birthday;

    private String rg;

    private String email;

    private boolean active;

    public RecoverClientDTO(Client cliente) {
        this.email = cliente.getEmail();
        this.name = cliente.getName();
        this.rg = cliente.getRg();
        this.birthday = cliente.getBirthday();
        this.active = cliente.isActive();
    }

}
