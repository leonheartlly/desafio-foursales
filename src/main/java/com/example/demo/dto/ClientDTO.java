package com.example.demo.dto;

import com.example.demo.domain.Client;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientDTO {

    @JsonIgnore
    @Nullable
    private Long id;

    @NotEmpty(message = "Nome não pode ser vazio.")
    private String name;

    private LocalDate birthday;

    private String password;

    @NotNull
    @CPF
    private String cpf;

    @NotEmpty(message = "Rg não pode ser vazio.")
    @Email
    private String rg;

    @NotEmpty(message = "Email não pode ser vazio.")
    @Email
    private String email;

    public ClientDTO(Client cliente) {
        this.cpf = cliente.getCpf();
        this.email = cliente.getEmail();
        this.name = cliente.getName();
        this.rg = cliente.getRg();
        this.birthday = cliente.getBirthday();
    }

}
