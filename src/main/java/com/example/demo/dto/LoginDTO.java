package com.example.demo.dto;

import com.example.demo.domain.Client;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginDTO {

    @NotEmpty(message = "Email não pode ser vazio.")
    @Email
    private String email;

    @NotEmpty(message = "Senha não pode ser vazio.")
    private String password;

    public LoginDTO(Client cliente) {
        this.email = cliente.getEmail();
        this.password = cliente.getPassword();
    }

}
