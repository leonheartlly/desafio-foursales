package com.example.demo.domain.enuns;


import java.util.Optional;


public enum CardStatus {

    OK(1, "OK"),
    BLOCK(2, "BLOCK"),
    EXPIRED(3, "EXPIRED");

    private int code;

    private String desc;


    CardStatus(int code, String desc) {

        this.code = code;
        this.desc = desc;
    }


    public int getCode() {

        return code;
    }


    public String getDesc() {

        return desc;
    }


    public static CardStatus getPedidoStatus(Integer cod) {

        if (!Optional.ofNullable(cod).isPresent()) {
            return null;
        }

        for (CardStatus ps : CardStatus.values()) {
            if (cod.equals(ps.getCode())) {
                return ps;
            }
        }
        throw new IllegalArgumentException("Id inválido: " + cod);
    }

}
