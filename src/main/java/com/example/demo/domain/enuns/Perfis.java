package com.example.demo.domain.enuns;

import java.util.Optional;

public enum Perfis {

    ADMIN(1, "ROLE_ADMIN"),
    CLIENT(2, "ROLE_CLIENT");

    private int code;

    private String desc;


    Perfis(int code, String desc) {

        this.code = code;
        this.desc = desc;
    }


    public int getCode() {

        return code;
    }


    public String getDesc() {

        return desc;
    }


    public static Perfis getPedidoStatus(Integer cod) {

        if (!Optional.ofNullable(cod).isPresent()) {
            return null;
        }

        for (Perfis ps : Perfis.values()) {
            if (cod.equals(ps.getCode())) {
                return ps;
            }
        }
        throw new IllegalArgumentException("Id inválido: " + cod);
    }

}
