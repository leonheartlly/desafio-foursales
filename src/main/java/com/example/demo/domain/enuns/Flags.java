package com.example.demo.domain.enuns;


import java.util.Optional;


public enum Flags {

    VISA(1, "VISA"),
    MASTER(2, "MASTER"),
    MAESTRO(3, "MAESTRO");

    private int code;

    private String desc;


    Flags(int code, String desc) {

        this.code = code;
        this.desc = desc;
    }


    public int getCode() {

        return code;
    }


    public String getDesc() {

        return desc;
    }


    public static Flags getPedidoStatus(Integer cod) {

        if (!Optional.ofNullable(cod).isPresent()) {
            return null;
        }

        for (Flags ps : Flags.values()) {
            if (cod.equals(ps.getCode())) {
                return ps;
            }
        }
        throw new IllegalArgumentException("Id inválido: " + cod);
    }

    public static Flags getPedidoStatus(String flag) {

        if (Optional.ofNullable(flag).isEmpty()) {
            return null;
        }

        for (Flags ps : Flags.values()) {
            if (flag.equals(ps.getDesc())) {
                return ps;
            }
        }
        throw new IllegalArgumentException("Id inválido: " + flag);
    }

}
