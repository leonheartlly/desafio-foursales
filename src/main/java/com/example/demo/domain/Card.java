package com.example.demo.domain;

import com.example.demo.domain.enuns.CardStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Card implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column(name = "display_name")
    public String displayName;

    private String cpf;

    @Enumerated(EnumType.STRING)
    private CardStatus status;

    @Column(name = "numero_cartao")
    private String cardNumber;

    @Column
    private int cvv;

    private String password;

    @Column
    private String flag;

    @Column
    private Integer attempt;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "data_expiracao")
    private LocalDate expirationDate;

    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    @JsonManagedReference
    private Client client;


    @PrePersist
    private void cardRulesBeforePersist(){
        creationDate = LocalDate.now();
        expirationDate = LocalDate.of(creationDate.getYear(), creationDate.getMonth(), creationDate.getDayOfMonth()).plusYears(8L);
        attempt = 0;
        status = CardStatus.BLOCK;
        id = null;
    }

}
