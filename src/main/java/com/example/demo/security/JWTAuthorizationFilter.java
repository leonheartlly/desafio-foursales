package com.example.demo.security;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private TokenAuthenticationComponent tokenAuthenticationComponent;
    private UserDetailsService userDetailsService;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager, TokenAuthenticationComponent tokenAuthenticationComponent, UserDetailsService userDetailsService) {
        super(authenticationManager);
        this.tokenAuthenticationComponent = tokenAuthenticationComponent;
        this.userDetailsService = userDetailsService;
    }

    /**
     * Intercepta a requisicao para validacao do token
     *
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        String header = request.getHeader("Authorization");

        if (StringUtils.isNotBlank(header) && header.startsWith("Bearer ")) {
            UsernamePasswordAuthenticationToken auth = getAthentication(request, header.substring(7));
            if (auth != null) {
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        }
        chain.doFilter(request, response);
    }

    /**
     * Recuera o token e valida se o usuario existe na base.
     *
     * @param request
     * @param token
     * @return
     */
    private UsernamePasswordAuthenticationToken getAthentication(HttpServletRequest request, String token) {
        if (tokenAuthenticationComponent.validToken(token)) {
            String username = tokenAuthenticationComponent.getUsername(token);
            UserDetails user = userDetailsService.loadUserByUsername(username);
            return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        }
        return null;
    }
}
