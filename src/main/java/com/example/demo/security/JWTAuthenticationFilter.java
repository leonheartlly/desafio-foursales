package com.example.demo.security;

import com.example.demo.domain.Client;
import com.example.demo.dto.ClientDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    private TokenAuthenticationComponent tokenAuthenticationComponent;


    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, TokenAuthenticationComponent tokenAuthenticationComponent) {
        this.authenticationManager = authenticationManager;
        this.tokenAuthenticationComponent = tokenAuthenticationComponent;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse resp) throws AuthenticationException {

        try {
            ClientDTO cli = new ObjectMapper()
                    .readValue(req.getInputStream(), ClientDTO.class);

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(cli.getEmail(), cli.getPassword(), new ArrayList<>());
            Authentication auth = authenticationManager.authenticate(authToken);

            return auth;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void successfulAuthentication(HttpServletRequest req, HttpServletResponse resp, FilterChain chain, Authentication auth) throws IOException, ServletException {

        String username = ((Client) auth.getPrincipal()).getEmail();
        String token = tokenAuthenticationComponent.generateToken(username);
        resp.addHeader("Authorization", "Bearer " + token);
    }
}
