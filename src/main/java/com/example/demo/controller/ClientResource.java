package com.example.demo.controller;


import com.example.demo.domain.Client;
import com.example.demo.dto.ClientDTO;
import com.example.demo.dto.RecoverClientDTO;
import com.example.demo.service.ClientService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping(value = "/desafio/clientes")
public class ClientResource {


    @Autowired
    private ClientService clientService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<RecoverClientDTO>> listAll() {

        List<Client> clients = clientService.listAll();
        List<RecoverClientDTO> clientDTOS = clients.stream().map(RecoverClientDTO::new).collect(Collectors.toList());

        return ResponseEntity.ok().body(clientDTOS);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ClientDTO> findById(@PathVariable Long id) {

        Client client = clientService.findById(id);
        ClientDTO clientDTO = new ClientDTO(client);

        return ResponseEntity.ok().body(clientDTO);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> add(@Valid @RequestBody ClientDTO clienteDTO) {

        clienteDTO.setId(null);
        Client client = clientService.convertDTO(clienteDTO);

        client = clientService.insert(client);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(client.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody ClientDTO clientDTO, @PathVariable Long id) {

        Client client = clientService.convertDTO(clientDTO);
        client.setId(id);

        clientService.update(client);

        return ResponseEntity.noContent().build();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {

        clientService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
