package com.example.demo.controller;


import com.example.demo.domain.Card;
import com.example.demo.dto.CardDTO;
import com.example.demo.dto.NewCardDTO;
import com.example.demo.service.CardService;
import javassist.tools.rmi.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping(value = "/desafio/cartoes")
public class CardResource {


    @Autowired
    private CardService cardService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<CardDTO>> listAll() {

        List<Card> pedidos = cardService.listAll();
        List<CardDTO> pedidoDTOS = pedidos.stream().map(obj -> new CardDTO(obj)).collect(Collectors.toList());

        return ResponseEntity.ok().body(pedidoDTOS);
    }

    @RequestMapping(value = "/client/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Card>> findByUserId(@PathVariable Long id) {

        List<Card> cards = cardService.findByClientId(id);

        return ResponseEntity.ok().body(cards);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<CardDTO> findById(@PathVariable Long id) {

        Card pedido = cardService.findById(id);
        CardDTO pedidoDTO = new CardDTO(pedido);

        return ResponseEntity.ok().body(pedidoDTO);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> add(@Valid @RequestBody NewCardDTO cardDTO) {
        Card card = cardService.insert(cardDTO);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(card.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody CardDTO cardDTO, @PathVariable Long id) throws ObjectNotFoundException {

        Card card = cardService.convertDTO(cardDTO);
        card.setId(id);

        card = cardService.update(card);

        return ResponseEntity.noContent().build();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {

        cardService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
