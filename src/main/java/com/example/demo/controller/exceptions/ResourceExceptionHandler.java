package com.example.demo.controller.exceptions;



import com.example.demo.service.Exceptions.DataIntegrityException;

import com.example.demo.service.Exceptions.BusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;


/***
 * Controla todas as excessões mapeadas recebidas através da classe de serviço.
 */
@ControllerAdvice
public class ResourceExceptionHandler {

    @ExceptionHandler( BusinessException.class )
    public ResponseEntity< StandardError > objectNotFound(BusinessException e, HttpServletRequest request ) {

        StandardError err = new StandardError( HttpStatus.NOT_FOUND.value(), e.getMessage(), System.currentTimeMillis() );

        return ResponseEntity.status( HttpStatus.NOT_FOUND ).body( err );
    }


    /***
     * Trata as excessões do tipo DataIntegrityException recebidas.
     * 
     * @param e
     * @param request
     * @return
     */
    @ExceptionHandler( DataIntegrityException.class )
    public ResponseEntity< StandardError > dataIntegrity(DataIntegrityException e, HttpServletRequest request ) {

        StandardError err = new StandardError( HttpStatus.BAD_REQUEST.value(), e.getMessage(), System.currentTimeMillis() );

        return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body( err );
    }


    /***
     * Trata as excessões do tipo DataIntegrityException recebidas.
     *
     * @param e
     * @param request
     * @return
     */
    @ExceptionHandler( MethodArgumentNotValidException.class )
    public ResponseEntity< StandardError > validation(MethodArgumentNotValidException e, HttpServletRequest request ) {

        ValidationError err = new ValidationError( HttpStatus.BAD_REQUEST.value(), "Erro de validação ", System.currentTimeMillis() );
        for ( FieldError f : e.getBindingResult().getFieldErrors() ) {
            err.addError( f.getField(), f.getDefaultMessage() );
        }
        return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body( err );
    }

    /***
     * Trata as excessões do tipo DataIntegrityException recebidas.
     *
     * @param e
     * @param request
     * @return
     */
    @ExceptionHandler( HttpMessageNotReadableException.class )
    public ResponseEntity< StandardError > enumException(HttpMessageNotReadableException e, HttpServletRequest request ) {

        ValidationError err = new ValidationError( HttpStatus.BAD_REQUEST.value(), e.getMessage(), System.currentTimeMillis() );

        return ResponseEntity.status( HttpStatus.BAD_REQUEST ).body( err );
    }

}
