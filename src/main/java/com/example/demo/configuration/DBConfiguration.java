package com.example.demo.configuration;

import com.example.demo.domain.Client;
import com.example.demo.dto.NewCardDTO;
import com.example.demo.domain.enuns.Flags;
import com.example.demo.service.CardService;
import com.example.demo.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;

@Configuration
public class DBConfiguration {

    @Autowired
    public ClientService clientService;

    @Autowired
    public CardService cardService;

    @Bean
    public void insertUsersOnDB(){

        Client jao = Client.builder()
                .name("joao da silva")
                .cpf("12345678910")
                .rg("123456")
                .email("joao.silva@outlook.com")
                .birthday(LocalDate.of(1987, Month.JANUARY, 5))
                .password("456789123")
                .build();

        clientService.insert(jao);

        Client ze = Client.builder()
                .name("Zezin de souza")
                .cpf("10987654321")
                .rg("654321")
                .email("zezin.souza@gmail.com")
                .birthday(LocalDate.of(1988, Month.AUGUST, 8))
                .password("159753")
                .build();

        clientService.insert(ze);

        Client maria = Client.builder()
                .name("Maria Antônia Joana de Habsburgo")
                .cpf("6467534675644")
                .rg("134687")
                .email("queen.france@outlook.com")
                .birthday(LocalDate.of(1755, Month.OCTOBER, 16))
                .password("456789123")
                .build();

        clientService.insert(maria);

        insertCardOnDB(jao, ze, maria);
    }

    public void insertCardOnDB(Client jao, Client ze, Client maria){
       NewCardDTO card1 =  NewCardDTO.builder()
            .flag(Flags.VISA)
            .id(jao.getId())
            .password("fadfadfa")
            .build();

        NewCardDTO card2 = NewCardDTO.builder()
            .flag(Flags.MAESTRO)
            .id(jao.getId())
            .password("fgsgfsgf")
            .build();

        cardService.insert(card1);
        cardService.insert(card2);

        NewCardDTO card3 =  NewCardDTO.builder()
            .flag(Flags.MASTER)
            .id(ze.getId())
            .password("4323432fsd")
            .build();

        cardService.insert(card3);

        NewCardDTO card4 =  NewCardDTO.builder()
            .flag(Flags.VISA)
            .id(maria.getId())
            .password("fadfadfa")
            .build();

        NewCardDTO card5 = NewCardDTO.builder()
            .flag(Flags.VISA)
            .id(maria.getId())
            .password("16/10 12:15")
            .build();

        cardService.insert(card4);
        cardService.insert(card5);
    }
}
